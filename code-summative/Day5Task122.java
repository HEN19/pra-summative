// inherit
class Day5Task122 extends Day5Task121 {
    protected int studentId;

    public Day5Task122(String name, int age, int studentId) {
        super(name, age);
        this.studentId = studentId;
    }

    public void displayStudentInfo() {
        System.out.println("Student Information:");
        displayInfo(); 
        System.out.println("Student ID: " + studentId);
    }
}

