public class Day5Task1 {
    private String studentName;
    private int age;

    private static int studentCount;

    public Day5Task1(String name, int age) {
        studentName = name;
        this.age = age;
        studentCount++;
    }

    public void displayStudentInfo() {
        System.out.println("Student Name: " + studentName);
        System.out.println("Age: " + age);
    }

    public static void displayStudentCount() {
        System.out.println("Total Students: " + studentCount);
    }

    public static void main(String[] args) {
        Day5Task1 student1 = new Day5Task1("John", 18);
        student1.displayStudentInfo();

        Day5Task1 student2 = new Day5Task1("Alice", 17);
        student2.displayStudentInfo();

        displayStudentCount();
    }
}
