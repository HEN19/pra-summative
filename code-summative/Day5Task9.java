import java.util.HashMap;
import java.util.Map;

public class Day5Task9 {
    public static void main(String[] args) {
        Map<String, Integer> studentScores = new HashMap<>();
        studentScores.put("John", 90);
        studentScores.put("Alice", 85);
        studentScores.put("Bob", 78);
        studentScores.put("Hafid", 95);

        System.out.println("Using HashMap:");

        
        for (Map.Entry<String, Integer> entry : studentScores.entrySet()) {
            System.out.println("Student: " + entry.getKey() + ", Score: " + entry.getValue());
        }
    }
}
