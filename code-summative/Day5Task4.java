public class Day5Task4 {
    private String[] subjects;

    public Day5Task4(String[] subjects) {
        this.subjects = subjects;
    }

    public void displaySubjectInfo() {
        System.out.println("Subjects:");
        for (int i = 0; i < subjects.length; i++) {
            System.out.println((i + 1) + ". " + subjects[i]);
        }
    }

    public static void main(String[] args) {
        String[] subjects = {"Mathematics", "Science", "English"};
        Day5Task4 subjectInfo = new Day5Task4(subjects);
        subjectInfo.displaySubjectInfo();
    }
}
