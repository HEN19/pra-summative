abstract class Day5Task101 {
    protected String name;
    protected int age;

    public  Day5Task101(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public abstract void displayInfo();
}
