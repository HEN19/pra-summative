// Grandparent class
class Day5Task14 {
    protected String name;
    protected int age;

    public Day5Task14(String name, int age) {
        this.name = name;
        this.age = age;
    }

    // Method to display information (will be overridden)
    public void displayInfo() {
        System.out.println("SchoolPerson Information:");
        System.out.println("Name: " + name);
        System.out.println("Age: " + age);
    }
}

// Parent class inheriting from SchoolPerson
class Student extends Day5Task14 {
    private int studentId;

    public Student(String name, int age, int studentId) {
        super(name, age);
        this.studentId = studentId;
    }

    // Method overriding the displayInfo method in the SchoolPerson class
    @Override
    public void displayInfo() {
        System.out.println("Student Information:");
        super.displayInfo();  // Calling the displayInfo method from the superclass
        System.out.println("Student ID: " + studentId);
    }
}

// Child class inheriting from Student
class UniversityStudent extends Student {
    private String major;

    public UniversityStudent(String name, int age, int studentId, String major) {
        super(name, age, studentId);
        this.major = major;
    }

    // Method overriding the displayInfo method in the Student class
    @Override
    public void displayInfo() {
        System.out.println("University Student Information:");
        super.displayInfo();  // Calling the displayInfo method from the superclass (Student)
        System.out.println("Major: " + major);
    }

    public static void main(String[] args) {
        UniversityStudent uniStudent = new UniversityStudent("Alice", 20, 12345, "Computer Science");
        uniStudent.displayInfo();
    }
}
