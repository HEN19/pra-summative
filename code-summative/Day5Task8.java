import java.util.ArrayList;

public class Day5Task8 {
    public static void main(String[] args) {
        ArrayList<String> fruits = new ArrayList<>();
        fruits.add("Apple");
        fruits.add("Banana");
        fruits.add("Orange");

        System.out.println("Using ArrayList:");

        
        for (String fruit : fruits) {
            System.out.println("Fruit: " + fruit);
        }
    }
}
