// inherit
class Day5Task123 extends Day5Task121 {
    private String major;

    public Day5Task123(String name, int age, int studentId, String major) {
        super(name, age, studentId);
        this.major = major;
    }

    public void displayUniversityStudentInfo() {
        System.out.println("University Student Information:");
        displayStudentInfo();  
        System.out.println("Major: " + major);
    }

    public static void main(String[] args) {
        Day5Task123 uniStudent = new Day5Task123("Alice", 20, 12345, "Computer Science");
        uniStudent.displayUniversityStudentInfo();
    }
}
