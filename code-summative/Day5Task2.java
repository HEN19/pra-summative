public class Day5Task2 {
    public static int calculateScore(int marks) {
        return (marks > 50) ? 1 : 0;
    }

    public static void main(String[] args) {
        int student1Marks = 60;
        int student2Marks = 45;

        int result1 = calculateScore(student1Marks);
        int result2 = calculateScore(student2Marks);

        System.out.println("Result for Student 1: " + result1);
        System.out.println("Result for Student 2: " + result2);
    }
}
