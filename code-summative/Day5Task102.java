public class Day5Task102 extends Day5Task101 {
    private int studentId;

    public Day5Task102(String name, int age, int studentId) {
        super(name, age);
        this.studentId = studentId;
    }

    @Override
    public void displayInfo() {
        System.out.println("Student Information:");
        System.out.println("Name: " + name);
        System.out.println("Age: " + age);
        System.out.println("Student ID: " + studentId);
    }

    
    public static void main(String[] args) {
        Day5Task102 student = new Day5Task102("John", 18, 12345);
        student.displayInfo();
    }
}
