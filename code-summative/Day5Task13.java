public class Day5Task13 {
    
    public void displayInfo(String name, int age) {
        System.out.println("Person Information:");
        System.out.println("Name: " + name);
        System.out.println("Age: " + age);
    }

    
    public void displayInfo(String name, int age, int studentId) {
        System.out.println("Student Information:");
        displayInfo(name, age); 
        System.out.println("Student ID: " + studentId);
    }

    public void displayInfo(String name, int age, int studentId, String major) {
        System.out.println("University Student Information:");
        displayInfo(name, age, studentId);  
        System.out.println("Major: " + major);
    }

    public static void main(String[] args) {
        Day5Task13 obj = new Day5Task13();
        
      
        obj.displayInfo("Hafid", 23);

        
        obj.displayInfo("Ayu", 23, 888889);

        
        obj.displayInfo("Bob", 22, 67890, "Computer Science");
    }
}
