public class Day5Task173 implements Day5Task171, Day5Task172{
    private String studentName;
    private int mathScore;
    private int scienceScore;

    public Day5Task173(String studentName, int mathScore, int scienceScore) {
        this.studentName = studentName;
        this.mathScore = mathScore;
        this.scienceScore = scienceScore;
    }

    // Overrides the displayInfo method from the DisplayInfo interface
    @Override
    public void displayInfo() {
        System.out.println("Student Information:");
        System.out.println("Name: " + studentName);
        System.out.println("Math Score: " + mathScore);
        System.out.println("Science Score: " + scienceScore);
    }

    
    @Override
    public void analyzeResult() {
        int totalScore = mathScore + scienceScore;
        System.out.println("Result Analysis:");
        System.out.println("Total Score: " + totalScore);

        if (totalScore >= 120) {
            System.out.println("Congratulations! The student passed.");
        } else {
            System.out.println("Sorry! The student did not pass.");
        }
    }

    public static void main(String[] args) {
       
        Day5Task173 student = new Day5Task173("John", 85, 92);
        student.displayInfo();
        student.analyzeResult();
    }
}

