import java.io.Serializable;

public class Day5Task11 implements Serializable {
 
    private String name;
    private int age;

    
    private transient String transientField;

    public Day5Task11(String name, int age, String transientField) {
        this.name = name;
        this.age = age;
        this.transientField = transientField;
    }

    public void displayInfo() {
        System.out.println("Name: " + name);
        System.out.println("Age: " + age);
        System.out.println("Transient Field: " + transientField);
    }

    public static void main(String[] args) {
        Day5Task11 obj = new Day5Task11("Hafid", 23, "This field will not be serialized");
        obj.displayInfo();

       
    }
}
