public class Day5Task7 {
    public static void main(String[] args) {
        int[] numbers = {1, 2, 3, 4, 5};

        System.out.println("Using Array:");

        
        for (int num : numbers) {
            System.out.println("Number: " + num);
        }
    }
}
