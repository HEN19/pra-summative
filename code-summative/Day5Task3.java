public class Day5Task3 {
    public static void main(String[] args) {
        int student1Result = 1;
        int student2Result = 0;

        switch (student1Result) {
            case 1:
                System.out.println("Student 1 passed.");
                break;
            default:
                System.out.println("Student 1 failed.");
        }

        switch (student2Result) {
            case 1:
                System.out.println("Student 2 passed.");
                break;
            default:
                System.out.println("Student 2 failed.");
        }
    }
}
