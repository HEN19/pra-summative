// Parent class inheriting from Person
class Day5Task152 extends Day5Task151 {
    protected int studentId;

    public Day5Task152(String name, int age, int studentId) {
        super(name, age);
        this.studentId = studentId;
    }
    
    @Override
    public void displayInfo() {
        super.displayInfo();  
        System.out.println("Student ID: " + studentId);
    }
}
