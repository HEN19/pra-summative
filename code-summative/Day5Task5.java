public class Day5Task5 {
    public static void main(String[] args) {
        String[] students = {"John", "Alice", "Bob", "Due","Blake" ,"Ao", "Nana","Xi"};
        int[] mathScores = {85, 90, 78, 80 , 99 ,56, 78, 82};
        int[] scienceScores = {92, 88, 75, 85, 90, 78, 80 , 99};

        for (int i = 0; i < students.length; i++) {
            System.out.println("Student: " + students[i]);
            System.out.println("Math Score: " + mathScores[i]);
            System.out.println("Science Score: " + scienceScores[i]);
            if (i >=6){
                System.out.println("============= Max Student================= ");
                break;
            }
        }
    }
}
