public class Day5Task121 {
    protected String name;
    protected int age;

    public Day5Task121(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public void displayInfo() {
        System.out.println("Person Information:");
        System.out.println("Name: " + name);
        System.out.println("Age: " + age);
    }
}
