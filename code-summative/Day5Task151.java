
class Day5Task151 {
    protected String name;
    protected int age;

    public Day5Task151(String name, int age) {
        this.name = name;
        this.age = age;
    }


    public void displayInfo() {
        System.out.println("Person Information:");
        System.out.println("Name: " + name);
        System.out.println("Age: " + age);
    }
}
